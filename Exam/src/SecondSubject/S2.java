package SecondSubject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class S2 {


    public static void main(String[] args) {
/*Implement a Java GUI application (with Swing and AWT) composed of two text fields and one button.

The first text field holds the name of a text file. When clicking the button the text input in the
second text field is written in the text file.
 */


        new Frame();
    }

    public static class Frame extends JFrame {
        JTextField textField1, textField2;
        JButton button;


        public void init() {
            this.setLayout(null);

            textField1 = new JTextField();
            textField1.setBounds(0, 0, 130, 130);

            textField2 = new JTextField();
            textField2.setBounds(150, 0, 150, 150);

            button = new JButton("Press to write!");
            button.setBounds(0, 150, 300, 150);

            button.addActionListener(new ButtonAction());

            add(textField1);
            add(textField2);
            add(button);
        }


        Frame() {
            setTitle("Ex2");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(300, 300);
            setVisible(true);
        }

        public class ButtonAction implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String fileName = Frame.this.textField1.getText();
                String fileText = Frame.this.textField2.getText();

                File fisier = new File(fileName);

                try {
                    FileWriter writer = new FileWriter(fileName);
                    writer.write(fileText);
                    writer.close();
                } catch (IOException ex) {

                    ex.printStackTrace();
                }
            }
        }


    }
}
