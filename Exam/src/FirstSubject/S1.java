package FirstSubject;

public class S1 {
    public static void main(String[] args) {
        E testE = new E();
        testE.metG(2);
        testE.compClass.metA();
        //C E and F - done

        B testB = new B();
        D testD = new D();
        testB.setAssocE(testE);
        testB.setAggregClass(testD);
        //B D - done

        A testA = new A();
        testA.met(testB);


    }

    public static class A {
        public void met(B b) {
            System.out.println("I met this person");//testing purp
        }
    }

    public static class B {
        private long t;

        public void x() {
        }

        private D aggregClass;
        private E assocE;

        public E getAssocE() {
            return assocE;
        }

        public void setAssocE(E assocE) {
            this.assocE = assocE;
        }

        public D getAggregClass() {
            return aggregClass;
        }

        public void setAggregClass(D aggregClass) {
            this.aggregClass = aggregClass;
        }
        //don't forget to instantiate this in main, or you won't have anything to pass to the set method
    }

    public static interface C {
    }

    public static class D {
    }

    public static class E implements C {
        public void metG(int i) {
        }

        F compClass = new F();
    }

    public static class F {
        public void metA() {
        }
    }
}
